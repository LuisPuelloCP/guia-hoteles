$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:3000
    });

    $('#contacto').on('show.bs.modal', function(e){
        console.log("El modal se está mostrando");
        $('#contactoBtn1').removeClass('btn-outline-success');
        $('#contactoBtn1').addClass('btn-success');
        $('#contactoBtn1').prop('disabled',true);

        $('#contactoBtn2').removeClass('btn-outline-success');
        $('#contactoBtn2').addClass('btn-success');
        $('#contactoBtn2').prop('disabled',true);

        $('#contactoBtn3').removeClass('btn-outline-success');
        $('#contactoBtn3').addClass('btn-success');
        $('#contactoBtn3').prop('disabled',true);

        $('#contactoBtn4').removeClass('btn-outline-success');
        $('#contactoBtn4').addClass('btn-success');
        $('#contactoBtn4').prop('disabled',true);

        $('#contactoBtn5').removeClass('btn-outline-success');
        $('#contactoBtn5').addClass('btn-success');
        $('#contactoBtn5').prop('disabled',true);

        $('#contactoBtn6').removeClass('btn-outline-success');
        $('#contactoBtn6').addClass('btn-success');
        $('#contactoBtn6').prop('disabled',true);

        $('#contactoBtn7').removeClass('btn-outline-success');
        $('#contactoBtn7').addClass('btn-success');
        $('#contactoBtn7').prop('disabled',true);

        $('#contactoBtn8').removeClass('btn-outline-success');
        $('#contactoBtn8').addClass('btn-success');
        $('#contactoBtn8').prop('disabled',true);

        $('#contactoBtn9').removeClass('btn-outline-success');
        $('#contactoBtn9').addClass('btn-success');
        $('#contactoBtn9').prop('disabled',true);

    });
    
    $('#contacto').on('shown.bs.modal', function(e){
        console.log("El modal se mostró");
    });

    $('#contacto').on('hide.bs.modal', function(e){
        console.log("El modal se oculta");
    });

    $('#contacto').on('hidden.bs.modal', function(e){
        console.log("El modal se ocultó");
        $('#contactoBtn1').removeClass('btn-success');
        $('#contactoBtn1').addClass('btn-outline-success');
        $('#contactoBtn1').prop('disabled',false);

        $('#contactoBtn2').removeClass('btn-success');
        $('#contactoBtn2').addClass('btn-outline-success');
        $('#contactoBtn2').prop('disabled',false);

        $('#contactoBtn3').removeClass('btn-success');
        $('#contactoBtn3').addClass('btn-outline-success');
        $('#contactoBtn3').prop('disabled',false);

        $('#contactoBtn4').removeClass('btn-success');
        $('#contactoBtn4').addClass('btn-outline-success');
        $('#contactoBtn4').prop('disabled',false);

        $('#contactoBtn5').removeClass('btn-success');
        $('#contactoBtn5').addClass('btn-outline-success');
        $('#contactoBtn5').prop('disabled',false);

        $('#contactoBtn6').removeClass('btn-success');
        $('#contactoBtn6').addClass('btn-outline-success');
        $('#contactoBtn6').prop('disabled',false);

        $('#contactoBtn7').removeClass('btn-success');
        $('#contactoBtn7').addClass('btn-outline-success');
        $('#contactoBtn7').prop('disabled',false);

        $('#contactoBtn8').removeClass('btn-success');
        $('#contactoBtn8').addClass('btn-outline-success');
        $('#contactoBtn8').prop('disabled',false);

        $('#contactoBtn9').removeClass('btn-success');
        $('#contactoBtn9').addClass('btn-outline-success');
        $('#contactoBtn9').prop('disabled',false);
    });
});